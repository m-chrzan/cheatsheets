# Image manipulation (other than with Image Magick)

## QR codes

Decode:

    zbarimg -q --raw qrcode.png

Create:

    qrencode -o <file.png> <string>

## SVG

Convert to PDF:

    inkscape -D word.svg -o word.pdf

Compatible with LaTeX (see also latex.md):

    inkscape -D word.svg -o word.pdf --export-latex
