# Manipulating images!

## Simple format conversion

    convert in.jpg out.png

## Resize

Fits image within given geometry, keeping the aspect ratio!

    convert in.jpg -resize 720x720 out.jpg

Force resize to be exactly the given geometry with `!`

    convert in.jpg --resize 720x720\! out.jpg

## Compress

### JPG

convert -strip -interlace Plane -gaussian-blur 0.05 -quality 85% source.jpg result.jpg

## Convert PDF

PDFs can be converted to images. Set `-density XxY` for better resolution.

## Options with enum arguments

List possible option values

    convert -list <option>
    # e.g.
    convert -list dither
