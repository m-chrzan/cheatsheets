# The find Unix utility

## Ignoring directories

    find . -not -ipath "dir/*"

## Regex match

    find . -regex pattern # matches whole path

## Formatted printing

    -printf <format>

Action that, instead of just printing the filename of found files, prints a
formatted string for each.

### Directives

* Time related:
    * `%A`: access time
    * `%C`: status change time
    * `%T`: modification time
  Each should be followed by a time format specifier, @ gets fractions UNIX time
* `%p`: filename
