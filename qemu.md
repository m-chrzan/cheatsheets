# The Qemu Hypervisor

## Create an image from ISO

    qemu-img create -f qcow2 <image name> <disk space>

    qemu-system-x86_64 \
      -cdrom <iso> \
      -drive "file=<image created above>,format=qcow2" \
      -enable-kvm \
      -m <memory> \
      -smp <# processors>"

Run the second command to e.g. go through the Ubuntu installer provided on the
ISO.

## Create a copy-on-write image from another image

    qemu-img create -f qcow2 -b <source image> <new image file name>
