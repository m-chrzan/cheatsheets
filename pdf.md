# PDF operations

## Merge

    gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=out.pdf in1.pdf in2.pdf

## Compress

    gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/default -dNOPAUSE -dQUIET -dBATCH -dDetectDuplicateImages -dCompressFonts=true -r150 -sOutputFile=output.pdf input.pdf

## Extract pages

    pdfjam --outfile out.pdf in.pdf 1,3,4-7

## Images to pdf

    convert in1.jpg in2.jpg out.pdf
