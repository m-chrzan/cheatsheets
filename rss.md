# On RSS feeds

As per https://stackoverflow.com/a/11787940

> The specification has lacked clarity regarding whether HTML is permitted in
> elements other than an item's description, leading to wide variance in how
> aggregators treat character data in other elements. This makes it especially
> difficult for a publisher to determine how to encode the characters "&" and
> "<", which must be encoded in XML.

`&amp;`, `&mdash;`, etc won't work in XML title tags. This will break some feed
readers entirely (e.g. Thunderbird). Fix by using hexcode (e.g. for mdash:
`&#x2014;`)
