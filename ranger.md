# Ranger tricks

## Bulkrename

    :bulkrename

Opens the selected filenames in vim for editing.

## Bookmarks

Like in vim

* `m<X>` - create bookmark X
* `` `<X> `` - jump to bookmark X

## Tabs

* `gn` - create new
* `gT`, `gt` - switch
* `gc` - close
* `uq` - restore tab
* `M-<n>` - go to nth tab (create new if not exists)

## Tags

Files can have a single arbitrary tag, persistent between restarts.

* `t` toggles/sets `*` tag
* `"<key>` toggles/sets `<key>` tag
