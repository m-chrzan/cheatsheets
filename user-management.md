# Managing Linux users

## Adding a user

    useradd [-m|--create-home] [-G|--groups <group,...>] -s <shell> <username>
    passwd <username>
    # as root, to add to sudoers
    visudo

## Adding user to a group

    gpasswd -a user group
