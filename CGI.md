# CGI scripting

## Inputs

GET requests: `$QUERY_STRING`

POST requests: in standard input

## Redirect

    Location: <url to redirect to>
