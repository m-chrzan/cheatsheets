# The Silver Searcher

## Print context

    ag -B <before_lines> -A <after_lines> 

## Ignore directories

    ag --ignore <pattern>

## Only files matching regex

    ag -G <pattern>
