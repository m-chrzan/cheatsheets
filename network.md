# Basic networking operations

## Basic networking info

    ifconfig

## Find process binding a port

    # specifically tcp
    lsof -i tcp:8000
    # any protocol
    lsof -i :8000

## DNS

DNS lookup:

    drill <hostname> [record type]


So to lookup TXT records,

    drill host.com TXT

## Connecting to WIFI with iwctl

    iwctl
    device list

Ensure device and adapter are turned on:

    device <device> set-property Powered on
    adapter <adapter> set-property Powered on

Begin scan (no output), list networks, connect:

    station <device> scan
    station <device> get-networks
    station <device> connect SSID
