# The Nginx HTTP server

## Check config syntax

    nginx -t

## Logs

Specify separate logging file for given server

    access_log /var/log/nginx/blog/access.log;

To set up rotating/compressed logs, use `logrotate`. Config in
`/etc/logrotate.d/nginx`. Run the following to reload config:

    logrotate -v -f /etc/logrotate.d/nginx
