# The date utility

    date +FORMAT

## UNIX timestamp

    date +%s

From timestamp to human readable:

    date -d @<timestamp>
