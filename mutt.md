# (Neo)Mutt

## GPG

There's two methods. Found it easier to configure the "classic method".

Copy `/usr/share/doc/mutt/samples/gpg.rc` to config dir and source it.

On Arch, had to change `/usr/libexec/neomutt/pgpewrap` to `/usr/bin/pgpewrap`.

After composing message, press `p` to select crypto actions.

## Search patterns

- `~C`: search in TO and CC fields
