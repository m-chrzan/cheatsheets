# The PostgreSQL DBMS

Default user, db: `postgres`

## User management

Everything should be done as user `postgres`.

New user:

    createuser --pwprompt mypguser

Change password:

    # in `psql`
    ALTER ROLE user WITH PASSWORD 'xxx';

## New database

    sudo su postgres
    createdb -O mypguser mypgdatabase

    # delete db
    dropdb mypgdatabase
