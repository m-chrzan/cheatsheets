# LANGUAGE pragma Haskell extensions

List: https://downloads.haskell.org/~ghc/7.0.3/docs/html/libraries/Cabal-1.10.1.0/Language-Haskell-Extension.html

## Using

Put as *first* line of file:

    {-# LANGUAGE Extension, ... -#}

## FlexibleContexts

Relax some restrictions on the form of the context of a type signature.

Normally typeclass constraints have to have type variable arguments.

    modify :: MonadState s m => (s -> s) -> m ()

With FlexibleContexts, can be any type (?)

    modifyFst :: MonadState (a, b) m => (a -> a) -> m ()
