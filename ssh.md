# SSH

## Key generation

    ssh-keygen -t rsa -C "email@addres.com" -b 4096

Change password for key:

    ssh-keygen -p <keyname>

## Setup login with public key

    ssh-copy-id [user@]host

## Execute commands and start shell

    ssh -t host 'commands; bash'

## Port tunneling

    ssh -N host -L <client port>:<address:port from server's POV>

`-N` is to not start an SSH session.

## Configuring remote machine to handle local terminal

Sometimes the remote doesn't know how to handle your local terminal. This can
lead to backspace, `clear`, and other things not working.

    infocmp $TERM > $TERM.terminfo
    scp $TERM.terminfo remote:~/
    # on remote
    tic -x $TERM.terminfo
