# LaTeX

## Lists

`itemize` for bullet lists, `enumerate` for numbered, `description` for
descriptive.

    \begin{itemize}
        \item First item
        \item Second item
    \end{itemize}

    \begin{description}
        \item [Description 1] First item
        \item [Description 2] Second item
    \end{itemize}

    \begin{enumarate}[(a)]  % pick enumeration style
        \item First item
        ...
    \end{enumarate}

## Arrows

Forward implication:

    \implies

Backwards implication:

    \impliedby

## Figures

    \begin{figure}
        \centering
        \def\svgwidth{\columnwidth}
        \input{input.pdf_tex}
        \caption{
            Description
        }\label{ref-label}
    \end{figure}

Note, `\label` comes after `\caption`.

To embed an svg:

    inkscape -D word.svg -o word.pdf --export-latex
