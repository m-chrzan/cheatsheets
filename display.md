# Display management

## DPMS

Display Power Management Signaling controls power saving features on displays.

Query current settings:

    xset q

Turn DPMS on/off:

    xset -dpms # screen will not timeout
    xset dpms

Force screen poweroff:

    xset dpms force off
    xset dpms force standby
    xset dpms force suspend

Off/standby/suspend are different power-off modes. They have trade-offs in power
consumption vs. time to restart.
