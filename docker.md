# Docker containers

## `docker run`

* `--rm` will remove the Docker container after running the command (otherwise
  have to do it manually if you want to run another one-shot command)
* `-v <local path>:<container path>`: mount a volume
* `--entrypoint <command>`: use custom entry point, not from Dockerfile. Specify
  just the command, give arguments after specifying image.

## `docker exec`

Runs a command against a *running* container.
