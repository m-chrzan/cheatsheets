# Python

## Virtual Envs

Create:

    python3 -m venv <env name>

Run:

    source <env name>/bin/activate

### requirements.txt

Create based on currently installed packages:

    pip freeze > requirements.txt

Install from requirements:

    python -m pip install -r requirements.txt

## Executing a package

Create `./package/__main__.py` script you want to execute. Then

    python -m package

## Jupyter Notebook

Setting default browser:

    # generates a config file under ~/.jupyter/
    jupyter notebook --generate-config
    # edit the newly generated config
    c.NotebookApp.browser = '<browser command> %s'

Using a venv:

    # once in venv
    pip install ipykernel
    python -m ipykernel install --user --name <name for the env in Jupyter>
    # then select the new kernel in the web app
