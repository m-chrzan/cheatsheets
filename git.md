# Git tips and tricks

## Setup name and email

    git config --global user.name "Name"
    git config --global user.email "name@place.com"

## Delete remote branch

    git push -d <remote> <branch>

## `git bisect`

    git bisect start
    git bisect new
    git checkout <some commit a while ago>
    git bisect old
    git bisect run <command that returns 1 on old, 0 on new>

## Patches

    git diff > change.patch
    patch -p1 < change.patch # -p1 to remove the a/ and b/ from filepaths

## Exporting a commit

    git archive <commit>

Outputs to stdout a tar archive of the index at the given commit. Make a
directory of just the code, not a repo:

    git archive <commit> | tar --extract --directory <output dir>

## Remotes

Change remote url:

    git remote set-url <remote> <new url>
