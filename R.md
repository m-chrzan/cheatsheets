# R and R Markdown

## Installing packages

In R shell:

    install.packages(<package name>)

## Include a file

    source('<filename>')

## Compiling R Markdown

    require(rmarkdown)
    render('<filename>') # defaults to HTML
    render('<filename>' output_format='pdf_document')

## R Markdown

Run R code:

    ```{r}
    <code>
    ```

Run code but don't show code block:

    ```{r, echo = False}
    <code>
    ```
