# Writing Makefiles

## Pattern rules

In prerequesite, use exactly one `%`. Can then be used in requirements. E.g.

    %.o: %.c
        # compile .c file to .o

Details: <https://www.gnu.org/software/make/manual/html_node/Pattern-Rules.html>

## Automatic variables

* `$@`: current target
* `$<`: first prerequisite
* `$^`: all prerequisites, separated by spaces
* `$(@F)`: file-within-directory part of target path

More: <https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html>

## Text functions

* `$(var:suffix=replacement)`, syntax sugar for
  `$(patsubst %suffix, $replacement, $(var))`

More: <https://www.gnu.org/software/make/manual/html_node/Text-Functions.html>

## Filename functions

<https://www.gnu.org/software/make/manual/html_node/File-Name-Functions.html>

## Quiet commands

By default, `make` prints the command being run. Prepend `@` to silence this.
