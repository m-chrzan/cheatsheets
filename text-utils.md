# Various small text manipulation utilities

## `cut`

Select fields from each line based on some delimeter

    cut -d <delimiter> -f <list of fields>

Setting a different output delimiter:

    --output-delimiter <delimiter>

## `comm`

    comm file1 file2

Outputs three columns:

1. Lines unique to `file1`
2. Lines unique to `file2`
3. Lines in both files

Assumes files are sorted.

Suppress a column with e.g. `-1`.

So intersection of two files:

    comm -12 file1 file2

## `diff`

Side by side:

    diff -y
