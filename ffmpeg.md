# ffmpeg tricks

## Convert

Works for audio, too.

    ffmpeg -i in.EXT1 out.EXT2

## Cut

    ffmpeg -i in.mp4 -codec:v copy -codec:a copy -ss START_TIME -t DURATION out.mp4
    ffmpeg -ss START_TIME -t DURATION -i in.mp4 -codec:v copy -codec:a copy out.mp4

## Resize

    ffmpeg -i in.mp4 -s 720x480 out.mp4
    # -1 means to automatically preserve aspect ratio
    ffmpeg -i in.mp4 -filter:v scale=720:-1 out.mp4

## Audio/video delay

    ffmpeg -i "$input" -itsoffset <offset in seconds> -i "$input" -map 1:v -map 0:a -c:a copy -c:v libx264 "$output"

For reverse offset, swap 1 and 0.

Basically, take the input once with an offset, once without. Take the audio from
one of them, video from the other.

## Rotate

    # 90 degrees clockwise
    ffmpeg -i in.mov -vf "transpose=1" out.mov
    # 180 degrees
    ffmpeg -vf "transpose=2,transpose=2"

For the transpose parameter you can pass:

    0 = 90CounterCLockwise and Vertical Flip (default)
    1 = 90Clockwise
    2 = 90CounterClockwise
    3 = 90Clockwise and Vertical Flip

## Record screen

    ffmpeg -f x11grab -s 1920x1080 -i :0.0 out.mkv

## Record from webcam

    ffmpeg -i /dev/video0 out.mkv

## Record sound

    ffmpeg -f alsa -i default out.flac

## Concatanate audio

    ffmpet -i in1.mp3 -i in2.mp3 -i in3.mp3 -filter_complex '[0:0][1:0][2:0]concat=n=3:v=0:a=1' output.mp3

## Fitlering

### Filtergraph

* A filtergraph is a `;`-separated list of filterchains.
* A filterchain is a `,`-separated list of filters.
* A filter is

    [in1] ... [ink] filter_name=option1=val1:...:optionn=valn [out1] ... [outm]

* Use `-filter_complex` if more than one input or output stream.
