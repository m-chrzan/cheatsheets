# HTML stuff

## DOM manipulation

    // create new element
    document.createElement('button')
    
    // append child
    element1.appendChild(element2)
    
## Elements

### Button

By default, a button has type `reset` or `submit`, both of which reload the
page. To avoid reloading:

    <button type='button'></button>

### Checkbox

To check by default:

    <input type='checkbox' checked>

### Built-in collapsable

    <details>
        <summary>Description</summary>
        The rest of this text will be hidden until this is expanded.
    </details>

### Definitions

    <dl>
        <dt>Term</dt>
        <dd>Defintion of term</dd>
        ...
    </dl>

## CLI parsing with `pup`

`pup` is a little command line parser, inspired by `jq`.

    pup --file <file> '<selectors> [display function]'

By default pretty prints the full selected HTML elements. Setting display
function to `text{}` prints out just the inner text.

### Extracting attributes

Use the `attr` display function.

    pup ... audio attr{src}
