# Gradle - Java builds and dependency management

## Dependencies

Local jar:

    dependencies {
        implementation files('path/to/lib.jar')
    }

## Testing

Run only specified tests.

    ./gradlew test --tests <ClassName[.methodName]>

## Conventions

Given a root project name (in settings.gradle, rootProject.name), creates a
directory structure of

    src/
        main/
            java/
                root/package/name
            resources/
        test/
            java/
                root/package/name
            resources/

E.g. main code for com.example.project will be under `src/main/java/com/example/project`.

## Under the hood

Dependencies are saved to `~/.gradle/caches/modules-2/files-2.1/`
