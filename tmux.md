# Tmux

## Sessions

    (/) switch between sessions

## Window management

    .N  move window to index N
    w   choose window interactively

## Pane management

    q   display pane indices
    :move-pane -t <session id>:<window id>

## Run command in new session

    tmux new-session \; send-keys 'command'
    #  can be entered with Ctrl-V + Enter

## Run command in multiple panes at once

* `:setw synchronize-panes on`
    turn off with `:setw synchronize-panes off`

## Packages

First clone TPM:

    $ git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

Then in config:

    set -g @plugin 'tmux-plugins/tpm'
    set -g @plugin 'tmux-plugins/tmux-sensible'
    run '~/.tmux/plugins/tpm/tpm'

Then `<prefix>+I` to download listed plugins.
