# xrandr

## Use second screen

    xrandr --output HDMI1 --auto --above eDP1

## Turn off second screen

    xrandr --output HDMI1 --off
