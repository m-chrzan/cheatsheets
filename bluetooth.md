# Using Bluetooth on Linux

`bluez` and `bluez-utils` packages. `pulseaudio-blootooth` for audio.

Start `bluetooth` service.

## `bluetoothctl`

* `scan on` to search for unpaired devices
* `devices` to list known devices
* `pair <MAC>` to pair with device
* `connect <MAC>` to connect
* `disconnect <MAC>` to disconnect
* might need to `trust <MAC>`
