# GPG Crypto

## General options

* `--armor` or `-a`: ASCII armor, output is printable ASCII
* `--local-user` or `-u`: specify which ID to use

## Symmetric encryption

### Encrypt file with passphrase

    gpg --symmetric <file>

Add `--armor` to make it readable-ASCII encoded.

### Decrypt

    gpg --output <out-file> --decrypt <in-file>

## Export private key

    gpg --armor --export-secret-keys <ID>

## Export public key

    gpg --armor --export <ID> [--output <file>]

## Import key

    gpg --import <key file>

Receiving a key from key server:

    gpg --recv-key "<key ID>"

## Asymmetric crypto

    gpg --encrypt [--sign] [-u <signer>] [--recipient <receipient>] file

## Key management

Add new email to key:

    gpg --edit-key <key>
    # This gives an interactive shell
    > adduid
    # interactive form to provide name, email, comment
    > save
    # saves and quits

Sign a key (changes trust from `unknown` to `full`), apparently necessary for
use in mutt:

    gpg --edit-key <key>
    > sign
    > save

    # specify the signing key (by default it's your first one)
    gpg --local-user <signing key> --edit-key <key to sign>

## Key Fingerprint/ID

The long hex fingerprint in `gpg --list-keys` is, as could be expected, a
standardized hash of the key info.

Sometimes a shorter ID is used -- this is actually a suffix of the full
fingerprint.

## gpg-agent

`gpg-agent` is a daemon that e.g. provides a passphrase cache for GPG. Running
enters an interactive shell. Commands can be also be passed on the command line
as individual arguments.

Configuration in `~/.gnupg/gpg-agent.conf`

* `/bye`: closes connection and shell
* `keyinfo --list`: lists some information about keys

    S KEYINFO FE40F504838C6FEDF80C0A592AE43EA8BFFC619E D - - - P - - -
    S KEYINFO 8E6D7DD22EA6A76CB7CB4E52315D4F372F67A23C D - - 1 P - - -

  The long identifier is a "keygrip", use `gpg --list-keys --with-keygrip` to
  match key with keygrip. The 6th column indicates if the passphrase is currently
  cached.
* `reloadagent`: restarts the agent.
