# Lilypond stuff

Docs: `info LilyPond`.

## Fine-grained page size

    #(set! paper-alist (cons '("custom size" . (cons (* 3.8 cm) (* 1.6 cm)))
                             paper-alist))
    \paper {
      #(set-paper-size "custom size")
    }

## Remove footer

    \header {
        tagline = ##f
    }

## Paper settings

Indentation of first line:

    \paper {
        indent = 0
    }

## Rhythm stuff

### Setting beat divisions

    \overrideTimeSignatureSettings
          4/4        % timeSignatureFraction
          1/4        % baseMomentFraction
          #'(3 1)    % beatStructure
          #'()       % beamExceptions
    \time 4/4
    \repeat unfold 8 { c8 } |
    \revertTimeSignatureSettings 4/4 % reverts to defaults
    \time 4/4
    \repeat unfold 8 { c8 } |

### Pickup measure

    \time 3/4
    \partial 8
    e8 | a4 c8 b c4 |

## Output formats

### Images (PNG/SVG)

    lilypond --svg file.ly
    lilypond --png file.ly

It's useful to have these cropped:

    lilypond --svg -dcrop file.ly

Will output both an A4-sized file.svg, and the cropped file.cropped.svg.
Suppress the non-cropped output with `-dno-print-pages`.

### MusicXML

Not supported natively, but the `python-ly` package on `pip` has experimental
support.

    ly musicxml file.ly

## Notation

Tie:

    a~ a
