# Systemd control

## Unit files

Find unit file for given service:

    systemctl status <service>

After editing unit file, need to reload wih

    systemctl daemon-reload

## Logs

    journalctl -u <service>
