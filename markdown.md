# Markdown/Pandoc tricks

## Formatting

Force a line break: empty line with `\`.

## Math

    $\text{\LaTeX}$

    \begin{equation}
        \mathbb{N} \ni 2 + 2 = 2^2
    \end{equation}

## Links

### Named links

    [This][link name] is a link. [Link name][] is the link's name.

    [link name]: <https://en.wikipedia.org> (link title)

### Automatic links

    <https://youtube.com>

## Images

    ![alt text](https://url.of/image.png)
    ![alt text][reference]

    [reference]: https://url.of/image.png

Can place images side-by-side and control width:

    ![](1.png){width=50%} ![](2.png){width=50%}

## Presentations

    pandoc in.md -t beamer -o out.pdf
    pandoc in.md -t beamer -o out.pdf -i # incremental lists
    pandoc in.md -t beamer --slide-level 2 -o out.pdf # # - section, ## - slide

### Markdown details

* `#` is a new slide with heading
* Metadata to generate title page, select theme, etc.:

        ---
        title:
        - Title
        subtitle:
        - Subtitle
        author:
        - Author Name
        - Another Author
        theme:
        - Copenhagen
        toc: true
        ---
