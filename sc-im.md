# The TUI Spreadsheet

Resources:

* <https://github.com/groessler/scim/blob/master/src/doc>
* <https://github.com/jonnieey/Sc-im-Tutorial>

## Basic editing

* `<`/`\`/`>`: insert left/center/right-aligned string
* `=`: insert numeric expression
* `e`: edit numeric
    * If accidentally added to text field, can erase with `=<RET>` (inserting an
      empty numeric value)
* `E`: edit string
* `ir`/`c`: insert row/column above/to the left
* `or`/`c`: insert row/column below/to the right

## Copying

* `mc`: (where `c` is a lowercase character) marks cell or range with `c`
* `cc`: (where the second `c` is a previous mark) copies content from mark into
  current cell, preserving formatting and adjusting formula.
* `yy`: copy current cell/selection
* `Pv`: paste only the value
* `Pf`: paste only the formatting
* `yr`/`c`: yank row/column
* `p`: paste below/on the left
* `t`: paste above/on the right

## Movement

* `hjkl`
* `^`/`#`: move to top/bottom of column (bottom is the last filled cell)
* `0`/`$`: move to left/right of row (rightmost is the last filled cell)
* `goc3`: go to cell C3
* `g0`/`$`: move to left/rightmost visible column
* `c-e`/`y`: scroll down/up maintaining cursor position
* `zh`/`l`: scroll left/right maintaining cursor position
* `c-f`/`b`: scroll page down/up

## Hiding/unhiding

"Z" for "zap" when hiding.

* `Z[cr]`: zap current column/row.
* `S[cr]`: shows hidden columns/rows in selected range (select with c-v)

## Formatting

* `c-j`: auto-fit column width
* `{`/`|`/`}`: align text left/center/right
* `f-`/`+`: decrease/increase decimal precision
* `f<`/`>`: decrease/increase column width

### Scientific notation

    :format "#.##e-#"

The two `#`s after the decimal point give precision. `e-` means scientific
notation, with sign only if the exponent is negative.
