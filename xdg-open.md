# Opening files with specific programs

`xdg-open` looks up a file's mimetype, then determines which application to use
to open it.

The local database for applications to use is in `~/.config/mimeapps.list`. Need
a `.desktop` file, which can be created under `~/.local/share/applications/`.

## Setting defaults

Easiest to do with `mimeopen` (from `perl-file-mimeinfo` on Arch):

    mimeopen -d <file>
    # will prompt to select a program from a list

## Default browser

The relevant mime types are:

    x-scheme-handler/http=
    x-scheme-handler/https=

For qutebrowser, just copied the `.desktop` from [the repo](https://github.com/qutebrowser/qutebrowser/blob/master/misc/org.qutebrowser.qutebrowser.desktop).
