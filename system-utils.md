# For basic OS utilities

* `mktemp`: creates a temporary file in /tmp and returns filename

## `mount`

Set file permissions of mounted drive at mount time:

    mount /dev/foo /mnt/bar -o umask=000
