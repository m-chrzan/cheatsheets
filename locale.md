# Locale tweaks

Every time after a clean install I have problems with multibyte (Polish)
characters not being handled by the terminal properly. To solve this: generate
both the `en_US` and `pl_PL` utf8 locales, then create a `.config/locale.conf`:

    LANG="pl_PL.utf8"
    LC_MESSAGES="en_US.utf8"
    LC_TIME="en_US.utf8"
    LC_NUMERIC="en_US.utf8"
    LC_MEASUREMENT="en_US.utf8"
    LC_MONETARY="en_US.utf8"
    LC_CTYPE="pl_PL.utf8"

## Polish font for console

    # /etc/vconsole.conf
    KEYMAP=pl2
    FONT=lat2-16
