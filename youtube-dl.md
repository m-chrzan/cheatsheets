# youtube-dl

## Only audio

    youtube-dl -x <url>

## Lower quality

    youtube-dl -f "[filesize<{SIZE}M]" <url>

## 720p

    youtube-dl -f "[height=720]" <url>
    youtube-dl -f "[height=720]+bestaudio" <url>
