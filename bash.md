# Bash scripting

## String manipulation

Remove shortest from end

    ${VAR%substr}
    # e.g.
    ${FILEPATH%.*}.out   # change extension from whatever to .out

Remove longest from start

    ${VAR##substr}
    # e.g.
    ${FILEPATH##*/}      # get only file name portion

%% - longest from end, # - shortest from start

## Control flow

Loop over files

    for file in glob/*; do something $file; done

Loop over integers

    for i in `seq 10`; do echo $i; done

## Conditionals

* `-z`: is empty string
* `-n`: non-empty string
* `-f`: is regular file
* `$A == $B`: string equality, accepts globs
* `$A != $B`: string inequality

## Heredocs

    cat << EOF > file
    bla bla
    EOF

Ignoring the initial indent:

    ...
        cat <<- EOF > file
        bla bla
        EOF
    ...

Don't interpolate variables:

    cat << 'EOF' > file
    bla bla
    EOF

## Arrays

Declaring indexed arrays:

    declare -a name

Associative arrays:

    declare -A name

Assignment:

    array=([bla]=foo [ble]=bar...)

All values:

    ${array[*]} # one word, elements separated with first character of $IFS
    ${array[@]} # separate words when double quoted

All keys:

    ${!array[*]}
    ${!array[@]}

## Commandline arguments

* `$#`: number of arguments.

## Getopts example

    while getopts 'a:b:c' flag; do
      case "${flag}" in
        a) do_something $OPTARG ;;
        b) b_option=$OPTARG ;;
        c) c_flag=1 ;;
        *) error "Unexpected option ${flag}" ;;
      esac
    done

### With positional arguments

Positional arguments will have to be supplied after flag options (`command
[options] <args>`):

    shift $((OPTIND-1))
