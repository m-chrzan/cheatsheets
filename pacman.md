# Arch Linux Packages

## Downgrade cached package

    pacman -U /var/cache/pacman/pkg/package-version...

## Debloating

### Cleaning cache

    paccache -r

* `-k`: how many past versions of each package to keep, defaults to 3.
* `-u`: target only uninstalled packages.

    # remove all versions of uninstalled packages
    paccache -ruk0

### Removing orphaned packages

    pacman -Qtdq | pacman -Rns -

### List packages by size

    LC_ALL=C pacman -Qi | awk '/^Name/{name=$3} /^Installed Size/{print $4$5, name}' | sort -h

## Troubleshooting failed installations

Usually enough to update keyring:

    pacman -Sy archlinux-keyring

Might need to update mirrorlist first if outdated:

    sudo pacman -Sy pacman-mirrorlist

(this might put it in /etc/pacman.d/mirrorlist.pacnew, uncomment wanted mirrors
there and remove the .pacnew suffix)

## Query (-Q)

By itself, outputs all installed packages.

## Browsing installed packages with `fzf`

    pacman -Qq | fzf --preview 'pacman -Qil {}' --layout=reverse --bind 'enter:execute(pacman -Qil {} | less)'
