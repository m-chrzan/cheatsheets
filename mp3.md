# Manipulating mp3s

## id3v2

For editing/viewing mp3 tags.

* `-a`, `--artist`
* `-A`, `--album`
* `-t`, `--song`
* `-y`, `--year`
* `-T`, `--track` (track number)

E.g.

    # Can pass multiple files
    id3v2 --artist "Blind Guardian" --album "At the Edge of Time" --year 2010 *

* `-l`, `--list`: lists all tags
