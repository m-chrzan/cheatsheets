# Sed, the standard stream editor

Typical substitution:

    sed -e 's/pattern/replacement/flags'

Can do multiple:

    sed -e 's/pattern/replacement/flags; s/bla/ble/'

Back references require extended regexes:

    sed -r -e 's/"(\w+)"/\1/'
