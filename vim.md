# Vim tricks

## vimgrep

Search through all files in a directory

    :vimgrep /pattern/ **/*

Open window with all instances:

    :copen

## Search

    \c - ignore case

## Settings

    ic - ignore case in searches

## readline

vi keybindings in various `readline` and similar CLIs:

    # ~/.inputrc
    set editing-mode vi

    # ~/.editrc
    bind -v

    # ~/.haskeline
    editMode: Vi

    # ~/.guile
    (use-modules (ice-9 readline))
    (activate-readline)

## Navigating help

| prefix | example       | context                                          |
| ------ | ------------- | ------------------------------------------------ |
| `:`    | `:h :r`       | ex command (command starting with a colon)       |
| `none` | `:h r`        | normal mode                                      |
| `v_`   | `:h v_r`      | visual mode                                      |
| `i_`   | `:h i_CTRL-W` | insert mode                                      |
| `c_`   | `:h c_CTRL-R` | ex command line                                  |
| `/`    | `:h /\r`      | search pattern (in this case, `:h \r` also works |
| `'`    | `:h 'ro'`     | option                                           |
| `-`    | `:h -r`       | Vim argument (starting Vim)                      |

## Registers

: is the "last command" register. @: reruns the last command.

## Marks

mA - capital letters are global, will jump to file
`a - jumps to line and column of mark, not just line

## v/V/<c-v> as operators

Most commands act line-wise (d2j deletes two lines). v makes things
character-wise, <c-v> makes them block-wise.

## Ranges

    g/regex/command # executes command on each line matching regex
    v/regex/command # executes command on each line not matching regex

## Ex commands

    :norm abc # runs the keys abc on each line in range
    :g/abc/norm dt; # deletes until semicolon on each line with "abc"

    :co <location> # copies range to location (can be ., +n, 'a)
    :mv <location> # same but moves

    :s/pattern/thing/c # replace, prompting at each replacement

## Visual mode

gv - selects the previous visual
g<c-a/x> - increment/decrement by 1 more on each selected line

    0          1            1
    0 <c-a> -> 1  g<c-a> -> 2
    0          1            3

## Using vim as a hex editor?

    :%!xxd
    :%!xxd -r

## Spellcheck

    :setlocal spell spelllang=en_us
    [/]s - previous/next marked word
    [/]S - previous/next bad word
    z=   - replacement suggestions

## Formatting

    gq

## Splits

* number before split shortcut - height of old window

Set window height/width (defaults to max):

    [n]^W _
    [n]^W |

Equalize window heights/widths:

    ^W =

Close window:

    ^W c

## Movements

    % - goes to matching bracket

## Time travel

    :earlier 5m
    :later 10m
