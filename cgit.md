# cgit, the hyperfast git web frontend

## Serving with nginx

    apt install cgit fcgiwrap

Start and enable `fcgiwrap.socket`

Nginx site configuration:

    server {
        server_name git.m-chrzan.xyz;
        root /usr/share/cgit;
        try_files $uri @cgit;

        location @cgit {
            include fastcgi_params;
            fastcgi_param       SCRIPT_FILENAME /usr/lib/cgit/cgit.cgi;
            fastcgi_param       PATH_INFO       $uri;
            fastcgi_param       QUERY_STRING    $args;
            fastcgi_param       HTTP_HOST       $server_name;
            fastcgi_pass        unix:/run/fcgiwrap.socket;
        }
    }

## Configuring cgit

In `/etc/cgitrc`. See `man cgitrc` for more.

Tell cgit where to look for repos:

    scan-path=/srv/git

Don't serve a logo image:
    logo=

Syntax highlighting (needs python3-pygments installed):

    source-filter=/usr/lib/cgit/filters/syntax-highlighting.py

Clone URLs (visible on the bottom of summary tab):

    enable-http-clone=1
    clone-url=https://<cgit domain>/$CGIT_REPO_URL

Basic config:

    root-title=<title>
    root-desc=<description for home page>
    enable-index-owner=0 # since it's only my code, no use to clutter with owner
    repository-sort=age # sort by most recent commit (default is alphabetical)

### Repo specific config

First, set

    enable-git-config=1

Now settings under

    [cgit]
        whatever = ...

will be used for `repo.whatever` settings in cgit.

## READMEs

Can specify a file to use under `/about`. Either directly in the repo's
directory, or `<branch>:<path>` to take a file from the index at a particular
commit.

    readme=:README.md

will use `README.md` from default branch.

To render markdown as HTML (needs python3-markdown installed):

    about-filter=/usr/lib/cgit/filters/html-converters/md2html

To override rendering method:

    repo.readme=:README.txt
    about-filter=/usr/lib/cgit/filters/html-converters/txt2html
