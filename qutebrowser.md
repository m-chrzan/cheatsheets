# Qutebrowser functionalities

## Mute tab

By default bound to Alt+m.

    :tab-mute

## Cookies

    :set content.cookies.accept no-3rdparty

    Supposedly breaks gmail. Change to `no-unknown-3rdparty` or back to `all` if
    it does.
